import os
import base64 as b64

version = '0.2.3'

class Element():
    def __init__(self,tag,content='',childElements=[],**kargs):
        '''HTML element'''
        self.tag=tag
        self.endtag=1
        if 'endtag' in kargs: self.endtag = kargs['endtag']
        self.childElements = childElements
        self.attr = {
            'class':None,
            'id':None,
            'style':None,
            'content':content,
        }
        for ky in kargs:
            if ky == '_class':
                self.attr['class'] = kargs[ky]
            else:
                self.attr[ky] = kargs[ky]

    def addChild(self,elm):
        self.childElements.append(elm)

    def html(self):
        # Start of tag
        h = '<%s' %self.tag
        # Tag Attributes
        for attr in self.attr:
            if not attr in ['content','childElements','endtag'] and self.attr[attr] != None:
                h += ' %s="%s"' %(attr,self.attr[attr])
        
        h += '>'
        
        # Content inside of tag
        h += self.attr['content']
        
        # Add Child Elements
        for child in self.childElements:
            h += '\n    '+child.html()
        if len(self.childElements) > 0: h += '\n'
        
        # End tag
        if self.endtag:
            h += '</%s>' %self.tag
        
        return h

class WebPage():
    def __init__(self,title='',fullPage=0):
        self.content = []
        self.js_files = []
        self.css_files = []
        self.css_text = ''
        self.fullpage = fullPage
        self.pagetitle = title
        self.header_html = ''
        self.addMode = 1
        
    def setStyle(self,css):
        self.css_text = css

    def setHeaderHtml(self,html):
        self.header_html = html

    def add(self,elm):
        self.content.append(elm)
    
    def addjsFile(self,jspath):
        self.js_files.append(jspath)
    
    def addcssFile(self,csspath):
        self.css_files.append(csspath)
    
    #---
    #---Elements
    def elm(self,tag,text='',add=1,**kargs):
        elm = Element(tag,text,**kargs)
        if add and self.addMode: self.add(elm)
        return elm

    def title(self,text,**kargs):
        '''Add a page title (h1)'''
        return self.elm('h1',text,**kargs)

    def heading(self,text,**kargs):
        '''Add a section heading (h2)'''
        return self.elm('h2',text,**kargs)

    def image(self,src=None,base64=0,bytesio=None,ext='png',**kargs):
        '''Add an image (base64=1 to embed as base64 image)'''
        i = self.elm('img',**kargs)
        b64_data = None
        if type(src) == str:
            i.attr['src'] = src
            if base64:
                with open(src,'rb') as f:
                    b64_data = b64.b64encode(f.read())
                ext = os.path.splitext(src)[-1][1:]
        elif bytesio != None:
            # Bytesio Source
            b64_data = b64.b64encode(bytesio.getvalue())
            
        if b64_data != None:
            i.attr['src']= u'data:image/'+ext+';base64, '+b64_data.decode()
        
        i.endtag=0
        return i
    
    def text(self,text,**kargs):
        '''Add a text section (div)'''
        return self.elm('div',text.replace('\n','<br>').replace('  ','&nbsp;&nbsp;'),**kargs)
    
    def link(self,text,url,**kargs):
        return self.elm('a',text,href=url,**kargs)
    
    def list(self,list,ordered=0,**kargs):
        '''Add a list (ordered=1 for an numbered list)'''
        tag = 'ul'
        if ordered: tag = 'ol'
        items = []
        for l in list:
            if isinstance(l,Element):
                items.append(Element('li','',childElements=[l]))
            else:
                items.append(Element('li',str(l)))
        e = self.elm(tag,'',childElements=items,**kargs)
        return e
    
    def table(self,table,colheader=None,rowheader=None,**kargs):
        '''Add a table'''
        def table_html():
            h = '<table'
            for ky in kargs:
                h += ' %s="%s"' %(ky, kargs[ky])
            h += '>'
            
            # Column Header
            if colheader != None:
                h += '\n  <thead><tr>'
                
                if rowheader != None:
                    h += '\n    <th></th>'
                    
                for col in colheader:
                    h += '\n    <th>%s</th>' %str(col)
                h += '\n  </tr></thead>'
            
            # Table Data
            for r in range(len(table)):
                rw = table[r]
                h += '\n  <tr>'
                
                # Row Header
                if rowheader != None:
                    h += '\n    <th>%s</th>' %str(rowheader[r])
                
                for col in rw:
                    if isinstance(col,Element):
                        txt = col.html()
                    else:
                        txt = str(col)
                    h += '\n    <td>%s</td>' %txt
                h += '\n  </tr>'
            
            h += '\n</table>'
            
            return h
        
        e = self.elm('table','',**kargs)
        e.html = table_html
        return e

    def hr(self,**kargs):
        '''Add horizontal line'''
        return self.elm('hr','',endtag=0,**kargs)

    def br(self,**kargs):
        '''Add line break'''
        return self.elm('br','',endtag=0,**kargs)

    def code(self,text,**kargs):
        '''Add code example block'''
        return self.elm('pre','<code>%s</code>' %text,**kargs)

    def js(self,code,**kargs):
        '''Add javascript code'''
        return self.elm('script',code,type='text/javascript',**kargs)

    def css(self,code,**kargs):
        '''Add a css style'''
        return self.elm('style',code,**kargs)

    #---Rendered Elements
    def md(self,text):
        '''Add markdown content'''
        import CommonMark
        
        def md_html():
            return CommonMark.commonmark(text)
        
        e = self.elm('','')
        e.html = md_html
        return e
    
    def mustache(self,template,templateDict):
        '''Add a mustache rendered template'''
        import pystache

        def ms_html():
            return pystache.render(template,templateDict)
        
        e = self.elm('','')
        e.html = ms_html
        return e

    #---
    #---Return Functions
    def getHeader(self):
        js_files = self.getjsFiles()
        css_files = self.getcssFiles()

        return """<!DOCTYPE html>
<html>
<head>
    <title>{0}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {3}{4}
    <style>
        {1}
    </style>
    {2}
</head>
<body>

""".format(self.pagetitle,self.css_text,self.header_html,css_files,js_files)

    def getjsFiles(self):
        js_files = ''
        for fl in self.js_files:
            js_files += '\n    <script src="%s" type="text/javascript"></script>' %fl
        return js_files
        
    def getcssFiles(self):
        css_files = ''
        for fl in self.css_files:
            css_files += '\n     <link href="%s" rel="stylesheet" media="screen">' %fl
        return css_files
        
    def getFooter(self):
        return '\n\n</body>\n</html>'
        
    def getHtml(self,fullPage=None):
        '''Generate html for all content'''
        if fullPage == None: fullPage = self.fullpage
        h = ''
        if fullPage:
            h = self.getHeader()
        else:
            h += self.getjsFiles()
            h += self.getcssFiles()
            h += '\n'
        if self.css_text !='':
            h += '<style>%s\n</style>\n' %('\n    '.join(['']+self.css_text.splitlines()))
        for elm in self.content:
            h += elm.html()+'\n'
        if fullPage: h += self.getFooter()
        return h
    
