# pywebpage - A Python html page generator

pywebpage is a module that generates an html webpage by building content elements in a pythonic way.  HTML knowledge is useful but not required to use this module.

- Easily generate html reports from your Python scripts
- generates html string (to use as you want)
- Python 2 and 3 compatible
- content is generated in the order it is added

## Quick Example

        import pywebpage
        wp = pywebpage.WebPage()
        wp.title('Page Title')     # create and add title to the page
        wp.text('Page content')    # create and add text to the page
        wp.getHtml() # Return html for the page

## Page Elements
A list of all the page elements

- **title**(text) - the page title (h1)
- **text**(text) - a text section (wrapped in a div)
- **image**(src, base64=0) 
    - src (str) - specify the path to an image (img) 
    - set base64=1 to embed the image as a base64 string
- **image**(bytesio, ext) - embed a bytesio object as a base64 image
    - bytesio - a bytesio object
    - ext (str) - image extension format (jpg,png,.etc)
- **heading**(text) - a section heading (h2)
- **link**(text, url) - create a link (a)
- **list**(list, ordered=0) - create a bulleted or numbered (set ordered=1) list
- **table**(table,colheader=None,rowheader=None) - create a table
    - table - 2d array
    - colheader - optional column header (a list)
    - rowheader - optional row header (a list)
- **hr**() - horizontal line
- **br**() - line break
- **code**(code) - code example (pre code)
- **js**(script) - add javascript code
- **css**(style) - add css style
- **md**(text) - add markdown formatted text (need CommonMark installed)
- **mustache**(template,templateDict) - add a mustache template section(need pystache installed)
- **elm**(tag, text='', add=1, childElements=[]) Element - specify your own element to add
    - tag - the html tag (Example: span)
    - text - the content inside the tags
    - add=1 - automatically add this element to the page content
    - childElements=[] - add other elements inside this element
    - the element object is returned

**All elements have keyward arguments that get added as html tag attributes**

## Page Functions
- **getHtml(fullPage=0)** - generate html for the whole page.
    - fullPage=0 - set the fullPage=1 to include the html, head, and body tags when generating the html
- **add**(elm) - add an element to the content
- **setStyle**(css) - embed the css (str) as the page style (fullPage mode only)
- **setHeaderHtml**(html) - add your own header html to the page (fullPage mode only)
- **addjsFile**(jspath) - add the path of a javascript file to include (at top of html)
- **addcssFile**(csspath) - add the path of a css file to include (at top of html)

## Element Object
All elements are derived from the Element object and therefore support the same features

- **Element** (tag,content='',childElements=[],**kargs)
    - **tag** - the html tag
    - **content** - the content inside the html tag
    - **childElements** - a list of other elements that are generated after the content
    - **kargs** - optional arguments (and extra attributes)
        - **endtag=1** - add a closing tag (default is true)
        - all other arguments are treated as attributes in the tag
    - **.addChild(elm)** - add another element as a child of this element
    - **.html()** - function to generate html of the element

## Advanced Topics

### Add Mode
By default pywebpage is in addMode where elements are automatically added to the content as they are created.  This can be turned off by:

        wp.addMode = 0   # turn off auto adding of elements

Alternatively, a single element can not be automatically added by using the add argument:

        txt =wp.text('some text',add=0)

To add elements manually (if they haven't been added yet)

        wp.add(txt)

You can also use this method to add elements to other elements

        l = wp.link('google','https://google.com',add=0)
        wp.list([l])  # add the link above to the list

The list and table support having other elements added to them.

### Attributes
To add extra attributes to the elements tag, just add them as keyword arguments

        wp.text('some text',id='myid',_class='myclass')
        # returns <div id="myid" class="myclass">some text</div>

*Note: for class, use _class (since that is a Python keyword)

### Child Elements
All elements can have child elements which append after the default content.